<?php
  namespace TomasZezula\WP\Plugin\MultiSite\ApproveSite\Model;

  class Registration {

    private $username;

    private $email;

    private $site;

    private $site_name;

    static function init() {
      return new Registration();
    }

    static function populate($username, $email, $site, $site_name) {
      $registration = new Registration();
      $registration->username = $username;
      $registration->email = $email;
      $registration->site = $site;
      $registration->site_name = $site_name;
      return $registration;
    }

    function get_username() {
      return $this->username;
    }

    function get_email() {
      return $this->email;
    }

    function get_site() {
      return $this->site;
    }

    function get_site_name() {
      return $this->site_name;
    }

    function get_domain() {
      $root_domain = parse_url(site_url())['host'];
      return $this->site.'.'.$root_domain;
    }
  }
?>
