<?php
  namespace TomasZezula\WP\Plugin\MultiSite\ApproveSite\Model;

  class RegistrationValidator {

    static function validate($registration) {

      $username = $registration->get_username();
      $email = $registration->get_email();
      $site_name = $registration->get_site_name();

      $errors = array();

      if (strpos($username, ' ') !== FALSE) {
        $errors['username_space'] = 'Username contains an empty space';
      }

      if (empty($username)) {
        $errors['username_empty'] = 'Username is mandatory';
      }

      if (username_exists($username)) {
        $errors['username_exists'] = 'Username already exists';
      }

      if (!is_email($email)) {
        $errors['email_format_invalid'] = 'Email format is invalid';
      }

      if (email_exists($email)) {
        $errors['email_exists'] = 'Email already exists';
      }

      if (empty($registration->get_site())) {
        $errors['site_empty'] = 'Site is mandatory';
      }

      if (domain_exists($registration->get_domain(), '/')) {
        $errors['domain_exists'] = 'Domain already exists';
      }

      if (empty($site_name)) {
        $errors['site_name_empty'] = 'Site name is mandatory';
      }

      return $errors;
    }
  }
?>
