<?php
  namespace TomasZezula\WP\Plugin\MultiSite\ApproveSite;

  use TomasZezula\WP\Plugin\MultiSite\ApproveSite\EmailSender as EmailSender;
  use TomasZezula\WP\Plugin\MultiSite\ApproveSite\PasswordGenerator as PasswordGenerator;

  class SiteActivator {

    static function activate_site($activation_key) {
      global $wpdb;

      // Prevent the default email notification
      add_filter('wpmu_welcome_notification', '__return_false');

      // Activate the site
      $signup_result = wpmu_activate_signup($activation_key);

      if (!array_key_exists('blog_id', $signup_result)) {
        return array(
          'success' => false,
          'message' => 'User activation failed. Are you trying to use an expired activation key?'
        );
      }

      // Fetch blog details
      $blog_id = $signup_result['blog_id'];
      $blog_details = get_blog_details($blog_id);

      // Fetch user details
      $user = get_user_by('id', $signup_result['user_id']);

      // Send the welcome email with instructions
      $to = $user->user_email;
      $subject = 'New Network Site: ' . $signup_result['title'];
      $body_template = 'views/email/new-user-notification.php';
      $site_url = 'http://' . $blog_details->domain;

      // Use the generated password or simply provide a link to a password generator?
      $use_password_generator = esc_attr(get_option('wpmu_site_approval_use_password_generator', 'false')) === 'true';

      $meta = array(
        'site_url' => $site_url,
        'username' => $user->user_login,
        'email' => $user->user_email,
        'site_login_page' => $site_url . '/wp-login.php',
        'use_password_generator' => $use_password_generator
      );

      if ($use_password_generator) {
        // Generate the password reset link
        $password_generator_url = PasswordGenerator::get_password_reset_url($user);
        $meta['password_generator_url'] = $password_generator_url;
      } else {
        // Use the password that's been automatically generated for the new signup
        $meta['password'] = $signup_result['password'];
      }

      $email_sent = EmailSender::send(
        $to, $subject, $body_template, $meta
      );

      return array(
        'success' => $email_sent,
        'meta' => $meta
      );
    }
  }
?>
