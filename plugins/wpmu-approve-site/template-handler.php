<?php
  namespace TomasZezula\WP\Plugin\MultiSite\ApproveSite;

  class TemplateHandler {

    /**
  	 * The array of templates that this plugin tracks.
  	 */
  	protected $templates;

    function __construct($templates) {
      // Init plugin templates
      $this->templates = $templates;
    }

    /**
  	 * Adds our template to the page dropdown for v4.7+
  	 *
  	 */
  	public function add_new_template( $posts_templates ) {
  		$posts_templates = array_merge( $posts_templates, $this->templates );
  		return $posts_templates;
  	}

    /**
  	 * Adds our template to the pages cache in order to trick WordPress
  	 * into thinking the template file exists where it doens't really exist.
  	 */
  	public function register_project_templates( $atts ) {

  		// Create the key used for the themes cache
  		$cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

  		// Retrieve the cache list.
  		// If it doesn't exist, or it's empty prepare an array
  		$templates = wp_get_theme()->get_page_templates();
  		if ( empty( $templates ) ) {
  			$templates = array();
  		}

  		// New cache, therefore remove the old one
  		wp_cache_delete( $cache_key , 'themes');

  		// Now add our template to the list of templates by merging our templates
  		// with the existing templates array from the cache.
  		$templates = array_merge( $templates, $this->templates );

  		// Add the modified cache to allow WordPress to pick it up for listing
  		// available templates
  		wp_cache_add( $cache_key, $templates, 'themes', 1800 );

  		return $atts;

  	}

    public function view_project_template( $template ) {

  		// Get global post
  		global $post;

  		// Return template if post is empty
  		if ( ! $post ) {
  			return $template;
  		}

  		// Return default template if we don't have a custom one defined
  		if ( ! isset( $this->templates[get_post_meta(
  			$post->ID, '_wp_page_template', true
  		)] ) ) {
  			return $template;
  		}

  		$file = plugin_dir_path( __FILE__).'templates/'. get_post_meta(
  			$post->ID, '_wp_page_template', true
  		);

  		// Just to be safe, we check if the file exist first
  		if ( file_exists( $file ) ) {
  			return $file;
  		} else {
  			die('No such file: '.$file);
  		}

  		// Return template
  		return $template;
  	}

    function add_templated_page($title, $slug, $template, $parent_id = 0) {

      global $user_ID;
      global $wpdb;

      $page_slug = 'wpmu-'.$slug;

      $page_id = $wpdb->get_var(
        $wpdb->prepare("SELECT ID from {$wpdb->posts} WHERE post_name = %s AND parent_id = %s AND post_status = 'publish'", $page_slug, $parent_id)
      );

      if ($page_id) {
        return $page_id;
      }

      $new_post = array(
            'post_title' => 'WPMU Site Approve: '.$title,
            'post_content' => '',
            'post_status' => 'publish',
            'post_date' => date('Y-m-d H:i:s'),
            'post_author' => $user_ID,
            'post_type' => 'page',
            'post_name' => $page_slug,
            'post_parent' => $parent_id
        );

      $post_id = wp_insert_post($new_post);

      if ($post_id) {
        update_post_meta($post_id, '_wp_page_template', $template);
      } else {
        die('Failed to create templated page: '.$template);
      }
      return $post_id;
    }

    function remove_templated_pages() {
      $page_slugs = array('wpmu-registration', 'wpmu-registration-approve', 'wpmu-registration-reject');
      global $wpdb;
      foreach ($page_slugs as &$page_slug) {
        $page_id = $wpdb->get_var(
          $wpdb->prepare("SELECT ID from {$wpdb->posts} WHERE post_name = %s AND post_type = 'page' AND post_status = 'publish'", $page_slug)
        );
        if ($page_id) {
          // Skip trash
          wp_delete_post($page_id, true);
        }
      }
    }

    function get_registration_url() {
      return get_site_url(null, 'wpmu-registration');
    }
  }
?>
