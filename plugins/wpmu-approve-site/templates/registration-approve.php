<?php
  /* Template Name: Custom Registration Approve Page */

  require_once( APPROVE_SITE__PLUGIN_DIR . 'email-sender.php' );
  require_once( APPROVE_SITE__PLUGIN_DIR . 'password-generator.php' );

  use TomasZezula\WP\Plugin\MultiSite\ApproveSite\SiteActivator as SiteActivator;

  get_header();

  if ($_GET) {

    if (current_user_can('setup_network') && is_super_admin()) {
      $activation_key = $wpdb->escape($_GET['key']);
      $activation_result = SiteActivator::activate_site($activation_key);

      if ($activation_result['success']) {
        $meta = $activation_result['meta'];
        require_once( APPROVE_SITE__PLUGIN_DIR . 'views/approval-success.php' );
      } else {
        $error_message = array_key_exists('message', $activation_result) ?
          $activation_result['message'] : 'User activation failed.';
        wp_die( __($error_message) );
      }
    } else {
      wp_die(__('Access denied'));
    }
  }
  get_footer();
?>
