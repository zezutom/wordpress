<?php
  /* Template Name: Custom Registration Page */
  get_header();
  global $wpdb;

  // Form validation
  if ($_POST) {
    $username = $wpdb->escape($_POST['username']);
    $email = $wpdb->escape($_POST['email']);
    $site = $wpdb->escape($_POST['site']);

    $errors = array();

    if (strpos($username, ' ') !== FALSE) {
      $errors['username_space'] = 'Username contains an empty space';
    }

    if (empty($username)) {
      $errors['username_empty'] = 'Username is mandatory';
    }

    if (username_exists($username)) {
      $errors['username_exists'] = 'Username already exists';
    }
    if (!is_email($email)) {
      $errors['email_format_invalid'] = 'Email format is invalid';
    }
    if (email_exists($email)) {
      $errors['email_exists'] = 'Email already exists';
    }

    if (count($errors) == 0) {
      echo '<p>your request 3, Thank you.</p>';

      // $new_domain = $site.'.localhost';
      // $blog_id = wpmu_create_blog( $new_domain, '/', $site, null , array( 'public' => 1 ) );

      // Disable user notification email
      add_filter('wpmu_signup_blog_notification', '__return_false');

      // Save a new signup
      $new_domain = $site.'.localhost';
      $new_domain_title = $site;

      wpmu_signup_blog($new_domain, '/', $new_domain_title, $username, $email, array());
      // wpmu_signup_user( $username, $email, array('domain' => $new_domain, 'title' => $new_domain_title) );
      // wpmu_signup_user($username, $email, array('add_to_blog' => $blog_id, 'new_role' => 'admin'));
      $key = $wpdb->get_var($wpdb->prepare("SELECT activation_key FROM {$wpdb->signups} WHERE user_login = %s AND user_email = %s", $username, $email));

      // Send an email to the admin
      $to = get_option('wpmu_site_approval_admin_email');
      $subject = get_option('wpmu_site_approval_email_subject');
      $approveUrl = get_site_url(null, 'wpmu-registration-approve?key='.$key);
      $rejectUrl = get_site_url(null, 'wpmu-registration-reject?key='.$key);
      $body = '<p>'.$username.' / '.$email.' / '.$site.'</p><p><a href="'.$approveUrl.'">Approve</a> or <a href="'.$rejectUrl.'">Reject</a></p>';
      $headers = 'Content-Type: text/html; charset=ISO-8859-1';
      wp_mail($to, $subject, $body, $headers);

    } else {
      print_r($errors);
    }
  }
  require_once( APPROVE_SITE__PLUGIN_DIR . 'views/registration-form.php' );

  get_footer();
?>
