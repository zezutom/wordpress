<?php
/* Template Name: Custom Registration Approve Page */

  header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");

  if ($_GET) {
    global $wpdb;

    // check_admin_referer('create-user', '_wpnonce_create-user');
    // if (!current_user_can('create_users')) {
    //   wp_die(__('Cheatin&#8217; uh?'));
    // }

    $key = $wpdb->escape($_GET['key']);

    // Activate the site
    wpmu_activate_signup($key);

    require_once( APPROVE_SITE__PLUGIN_DIR . 'views/approval-success.php' );
  }
?>
