<?php
  /* Template Name: Custom Registration Reject Page */
  get_header();

  if ($_GET) {

    if (!current_user_can('setup_network')) {
      wp_die(__('Access denied'));
    }

    global $wpdb;

    $key = $wpdb->escape($_GET['key']);

    // Fetch user details
    $user_details = $wpdb->get_row(
      $wpdb->prepare("SELECT user_login, user_email, domain FROM {$wpdb->signups} WHERE activation_key = %s", $key)
    );

    if (is_null($user_details)) {
      wp_die(__('Activation key '.$key.' not found. Has the signup already been removed?'));
    }

    // Delete the signup
    $signup_deleted = $wpdb->delete($wpdb->signups, array('activation_key' => $key));

    if ($signup_deleted) {
      // Capture info about the signup
      $meta = array(
          'activation_key' => $key,
          'domain' => $user_details->domain,
          'username' => $user_details->user_login,
          'email' => $user_details->user_email
      );
      require_once( APPROVE_SITE__PLUGIN_DIR . 'views/rejection-success.php' );
    } else {
      wp_die(__('Failed to delete activation key '.$key.'. Please check the database.'));
    }
  }
  get_footer();
?>
