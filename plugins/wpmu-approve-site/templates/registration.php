<?php
  /* Template Name: Custom Registration Page */
  global $wpdb;

  require_once( APPROVE_SITE__PLUGIN_DIR . 'model/registration-validator.php' );
  require_once( APPROVE_SITE__PLUGIN_DIR . 'model/registration.php' );
  require_once( APPROVE_SITE__PLUGIN_DIR . 'email-sender.php' );

  use TomasZezula\WP\Plugin\MultiSite\ApproveSite\Model\Registration as Registration;
  use TomasZezula\WP\Plugin\MultiSite\ApproveSite\Model\RegistrationValidator as RegistrationValidator;
  use TomasZezula\WP\Plugin\MultiSite\ApproveSite\EmailSender as EmailSender;

  $registration_success = '';

  // Form validation
  if ($_POST) {

    $registration = Registration::populate(
      $wpdb->escape($_POST['username']),
      $wpdb->escape($_POST['email']),
      $wpdb->escape($_POST['site']),
      $wpdb->escape($_POST['site_name'])
    );

    $errors = RegistrationValidator::validate($registration);

    if (count($errors) == 0) {

      // Disable user notification email
      add_filter('wpmu_signup_blog_notification', '__return_false');
      add_filter('wpmu_signup_user_notification', '__return_false');

      // Save a new signup
      wpmu_signup_blog(
        $registration->get_domain(), '/',
        $registration->get_site_name(),
        $registration->get_username(),
        $registration->get_email(),
        array()
      );

      // Fetch the activation key
      $key = $wpdb->get_var(
        $wpdb->prepare("SELECT activation_key FROM {$wpdb->signups} WHERE user_login = %s AND user_email = %s",
        $registration->get_username(), $registration->get_email())
      );

      // Email the admin
      $to = get_option('wpmu_site_approval_admin_email');
      $subject = get_option('wpmu_site_approval_email_subject');
      $body_template = 'views/email/new-registration-notification.php';
      $approve_url = get_site_url(null, 'wpmu-registration-approve?key='.$key);
      $reject_url = get_site_url(null, 'wpmu-registration-reject?key='.$key);

      $email_sent = EmailSender::send(
        $to, $subject, $body_template,
        array(
          'username' => $registration->get_username(),
          'email' => $registration->get_email(),
          'domain' => $registration->get_domain(),
          'site_name' => $registration->get_site_name(),
          'approve_url' => $approve_url,
          'reject_url' => $reject_url          
        )
      );

      if ($email_sent) {
        // empty registration
        $registration = Registration::init();
        // Show the success message
        $registration_success = 'Your request has been submitted. You will hear from us soon!';
      } else {
        $errors['registration_failure'] = 'Oops, something went wrong. Please try later.';
      }
    }
  } else {
    // empty registration
    $registration = Registration::init();
    $errors = array();
  }

  require_once( APPROVE_SITE__PLUGIN_DIR . 'views/registration-form.php' );
?>
