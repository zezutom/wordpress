<?php
namespace TomasZezula\WP\Plugin\MultiSite\ApproveSite;

class Settings {

  private $default_email_subject = 'New Registration';

  function __construct() {

  }

  function add_menu() {
    //create new top-level menu
    add_menu_page('Site Registration', 'Site Registration', 'administrator', __FILE__, array($this, 'site_registration_settings_page') );
    //call register settings function
    add_action( 'admin_init', array($this, 'register_site_registration_settings') );
  }

  function register_site_registration_settings() {
    //register our settings
    register_setting( 'site-registration-settings-group', 'wpmu_site_approval_admin_email' );
    register_setting( 'site-registration-settings-group', 'wpmu_site_approval_email_subject' );
    register_setting( 'site-registration-settings-group', 'wpmu_site_approval_use_password_generator');
  }
  function site_registration_settings_page() {
    require_once( APPROVE_SITE__PLUGIN_DIR . 'views/settings.php' );
  }
}
?>
