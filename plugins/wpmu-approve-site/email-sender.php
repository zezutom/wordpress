<?php
  namespace TomasZezula\WP\Plugin\MultiSite\ApproveSite;

  class EmailSender {

    static function send($to, $subject, $body_template, $meta) {
      ob_start();
      require_once( APPROVE_SITE__PLUGIN_DIR . $body_template );
      $body = ob_get_contents();
      ob_end_clean();

      $result = wp_mail($to, $subject, $body, 'Content-Type: text/html; charset=UTF-8');
      // TODO capture any errors, log them and return a custom error message
      return $result;
    }
  }
?>
