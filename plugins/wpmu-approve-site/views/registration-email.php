<html>
  <head>
    <style>
      <?php require_once( APPROVE_SITE__PLUGIN_DIR . 'views/css/main.css' ); ?>
    </style>
  </head>
  <body>
    <div class="contents">
      <p>New site request pending your approval.</p>
      <p>
        <table>
          <tr>
            <th>Username:</th>
            <td><?php echo $registration->get_username(); ?></td>
          </tr>
          <tr>
            <th>Email:</th>
            <td><?php echo $registration->get_email(); ?></td>
          </tr>
          <tr>
            <th>Site:</th>
            <td><?php echo $registration->get_domain(); ?></td>
          </tr>
          <tr>
            <th>Site Name:</th>
            <td><?php echo $registration->get_site_name(); ?></td>
          </tr>
        </table>
      </p>
      <p>
        <div class="wrapper">
          <div class="button approve">
            <a href="<?php echo $approve_url; ?>" class="button-text">Approve</a>
          </div>
          <div class="button reject">
            <a href="<?php echo $reject_url; ?>" class="button-text">Reject</a>
          </div>
        </div>
      </p>
      <p>Buttons not working?</p>
      <ul>
        <li>
          <p>Click the link below to approve:</p>
          <a href="<?php echo $approve_url; ?>"><?php echo $approve_url; ?></a>
        </li>
        <li>
          <p>Click the link below to reject:</p>
          <a href="<?php echo $reject_url; ?>"><?php echo $reject_url; ?></a>
        </li>
      </ul>
    </div>
  </body>
</html>
