<?php
  $use_password_generator = esc_attr(get_option('wpmu_site_approval_use_password_generator', 'false')) === 'true';
?>
<div class="wrap">
<h1>Site Registration</h1>

  <form method="post" action="options.php">
    <?php settings_fields( 'site-registration-settings-group' ); ?>
    <?php do_settings_sections( 'site-registration-settings-group' ); ?>
    <table class="form-table">
      <tr valign="top">
        <th scope="row">Email Address</th>
        <td><input type="text" name="wpmu_site_approval_admin_email" value="<?php echo esc_attr( get_option('wpmu_site_approval_admin_email', get_option('admin_email')) ); ?>" /></td>
      </tr>

      <tr valign="top">
        <th scope="row">Email Subject</th>
        <td><input type="text" name="wpmu_site_approval_email_subject" value="<?php echo esc_attr( get_option('wpmu_site_approval_email_subject', $this->default_email_subject) ); ?>" /></td>
      </tr>

      <tr valign="top">
        <th scope="row">Password</th>
        <td>
          <input
            type="radio"
            name="wpmu_site_approval_use_password_generator"
            value="false"
            <?php echo $use_password_generator ? '' : ' checked=checked'; ?>>Generate Password<p>
          <input
            type="radio"
            name="wpmu_site_approval_use_password_generator"
            value="true"
            <?php echo $use_password_generator ? ' checked=checked' : ''; ?>>Send Link to Password Generator
        </td>
      </tr>
    </table>

    <?php submit_button(); ?>
  </form>
</div>
