<html>
  <head>
    <style>
      <?php require( APPROVE_SITE__PLUGIN_DIR . 'views/css/main.css' ); ?>
    </style>
  </head>
  <body>
    <div class="contents">
      <div class="registration-form">
        <p>The registration request for the following site has been rejected.</p>
        <table>
          <tr>
            <th>Domain:</th>
            <td><?php echo $meta['domain'] ?></td>
          </tr>
          <tr>
            <th>Admin:</th>
            <td><?php echo $meta['username'] ?></td>
          </tr>
          <tr>
            <th>Email:</th>
            <td><?php echo $meta['email'] ?></td>
          </tr>
        </table>
      </div>
    </div>
  </body>
</html>
