<html>
  <head>
    <style>
      <?php require( APPROVE_SITE__PLUGIN_DIR . 'views/css/main.css' ); ?>
    </style>
  </head>
  <body>
    <div class="contents">
      <p>Howdy <?php echo $meta['username']; ?>,</p>
      <p>Your new Network site has been successfully set up at:</p>
      <p><?php echo $meta['site_url']; ?></p>
      <p></p>
      <p>You can log in to the administrator account with the following information:</p>
      <p></p>
      <p>Username: <?php echo $meta['username']; ?></p>
      <?php if ($meta['use_password_generator']) { ?>
        <p>Use the link below to create your password:</p>
        <p>
          <a href="<?php echo $meta['password_generator_url']; ?>">
            <?php echo $meta['password_generator_url']; ?>
          </a>
        </p>
      <?php } else { ?>
        <p>Password: <?php echo $meta['password']; ?></p>
      <?php } ?>
      <p></p>
      <p>Log in here: <a href="<?php echo $meta['site_login_page']; ?>">
        <?php echo $meta['site_login_page']; ?>
      </a></p>
      <p></p>
      <p>We hope you enjoy your new site. Thanks!</p>
      <p></p>
      <p>--The Team @ Network</p>
    </div>
  </body>
</html>
