# Wordpress Multisite Approve Site Plugin

This plugin implements a custom site registration based on admin approval.

User and Site registration built into the WP Multisite lacks an important approval step.
While the network admin is notified about new users and their sites, there is no way of 
for the admin to approve a new registration before the user account gets actually created.

Default WPMU registration:
- Network admin allows for the user and site registration
- A new user registers their site, visits the activation link emailed to her and she is in: both the user and the site are created
- Admin receives an email notification

This plugin's registration requires an admin approval. No users or sites are created unless the admin approves of the registration request:

- Applicants register through a custom registration form
- Each registration sends an email to the admin, who has to approve it
- Once approved, the user and her site are created as usual
- An email confirmation is sent to the user

## Installation

Download this repo as a ZIP and unzip the contents of `plugins/wpmu-approve-plugin` into the plugins directory of your Wordpress Multisite installation.

Log into `wp-admin` and and network-active the plugin.

![WPMU Site Registration Approval - Network Activation](https://gitlab.com/zezutom/wordpress/uploads/772028122978233f5ef1f01f37152645/wpmu_site_registration_approval_activation.png)

## Configuration

Once you active plugin, head to the `Site Registration` menu (appears on your bar, just below Settings).

* Email - email address where you will receive notifications about new signups. Defaults to the admin email address.
* Subject - notification subject, defaults to New Registration (you probably want to leave it unchanged)

![WPMU Site Registration Approval - Menu](https://gitlab.com/zezutom/wordpress/uploads/a50cb2004475e120144d16990e4436c3/wpmu_site_registration_approval_menu.png)

Lastly, head into `My Sites > Network Admin > Settings`, section `Registration Settings / allow new registrations` and tick `User accounts may be registered`.

Note: This is only necessary for the Registration link to show on the frontpage. The registration is custom anyway, I plan to 
remove this additional config step in the future.

![WPMU Site Registration Approval - WP Admin: User Registration](https://gitlab.com/zezutom/wordpress/uploads/ad0d390c266adccbee45bf1baee01ecb/wp_admin_user_registration_config.png)

## Screenshots

### Custom Registration Form
![WPMU Site Registration Approval - Registration Form](https://gitlab.com/zezutom/wordpress/uploads/dad8d89b07b5150d3eee31f64e8fc951/wpmu_site_registration_approval_reg_form.png)

### User Input Validation
![WPMU Site Registration Approval - Registration Form Validation](https://gitlab.com/zezutom/wordpress/uploads/089f84e2bb5c1dca45f33a62b4f0f35f/wpmu_site_registration_approval_reg_form_validation.png)

### Admin Notification Email
![WPMU Site Registration Approval - Admin Notification](https://gitlab.com/zezutom/wordpress/uploads/fbe9a7de6692aa8e4f3311a3cc9525c9/wpmu_site_registration_approval_admin_notification.png)

### User Confirmation Email upon an Approval
![WPMU Site Registration Approval - User Notification](https://gitlab.com/zezutom/wordpress/uploads/88f489287498f2bb622185051b0a1002/wpmu_site_registration_approval_user_notification.png)
