<?php
  namespace TomasZezula\WP\Plugin\MultiSite\ApproveSite;

  class PasswordGenerator {

    static function get_password_reset_url($user) {
      $password_reset_key = get_password_reset_key($user);
      $encoded_login = rawurlencode($user->user_login);
      return network_site_url( 'wp-login.php?action=rp&key='.$password_reset_key.'&login=' . $encoded_login, 'login' );
    }
  }
?>
