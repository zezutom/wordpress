<?php
  /*
      Plugin Name: WordPress MU Site Registration Approval
      Plugin URI:
      Description: New site registrations must be approved by the multisite admin
      Author: Tomas Zezula
      Author URI: https://www.tomaszezula.com
      Version: 0.1
      License: GPLv2 or later
      Text Domain: wpmu-approve-site-plugin
  */

  /*
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
  */

  namespace TomasZezula\WP\Plugin\MultiSite\ApproveSite;

  defined('ABSPATH') or die('Unauthorized access not allowed!');

  define( 'APPROVE_SITE__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
  require_once( APPROVE_SITE__PLUGIN_DIR . 'settings.php' );
  require_once( APPROVE_SITE__PLUGIN_DIR . 'template-handler.php' );
  require_once( APPROVE_SITE__PLUGIN_DIR . 'email-sender.php' );
  require_once( APPROVE_SITE__PLUGIN_DIR . 'site-activator.php' );
  require_once( APPROVE_SITE__PLUGIN_DIR . 'model/registration-validator.php' );
  require_once( APPROVE_SITE__PLUGIN_DIR . 'model/registration.php' );

  class ApproveSitePlugin {

    private $template_handler;

    function __construct($template_handler) {

      $this->template_handler = $template_handler;

      // Add a filter to the attributes metabox to inject template into the cache.
  		if ( version_compare( floatval( get_bloginfo( 'version' ) ), '4.7', '<' ) ) {
        // 4.6 and older
  			add_filter(
  				'page_attributes_dropdown_pages_args',
  				array( $this->template_handler, 'register_project_templates' )
  			);
      } else {
        // Add a filter to the wp 4.7 version attributes metabox
  			add_filter(
  				'theme_page_templates', array( $this->template_handler, 'add_new_template' )
  			);
      }

      // Add a filter to the save post to inject out template into the page cache
  		add_filter(
  			'wp_insert_post_data',
  			array( $this->template_handler, 'register_project_templates' )
  		);

      // Add a filter to the template include to determine if the page has our
  		// template assigned and return it's path
  		add_filter(
  			'template_include',
  			array( $this->template_handler, 'view_project_template' )
  		);

      // Create a custom registration link
      add_filter( 'register_url', array($this->template_handler, 'get_registration_url') );
    }

    function activate() {
      // Generate registration form, approve and reject pages
      $registration_page_id = $this->template_handler->add_templated_page(
        'Registration Page', 'registration', 'registration.php');

      $approval_page_id = $this->template_handler->add_templated_page(
        'Registration Page Approve', 'registration-approve', 'registration-approve.php', $registration_page_id);

      $rejection_page_id = $this->template_handler->add_templated_page(
        'Registration Page Reject', 'registration-reject', 'registration-reject.php', $registration_page_id);

      flush_rewrite_rules();
    }

    function deactivate() {
      // Remove plugin's pages
      $this->template_handler->remove_templated_pages();

      flush_rewrite_rules();
    }

    function uninstall() {
      // Delete all the plugin data from the DB
    }
  }


  // Verify that all plugin classes exist
  $required_classes = array(
    'TomasZezula\WP\Plugin\MultiSite\ApproveSite\ApproveSitePlugin',
    'TomasZezula\WP\Plugin\MultiSite\ApproveSite\Settings',
    'TomasZezula\WP\Plugin\MultiSite\ApproveSite\TemplateHandler',
    'TomasZezula\WP\Plugin\MultiSite\ApproveSite\EmailSender',
    'TomasZezula\WP\Plugin\MultiSite\ApproveSite\SiteActivator',
    'TomasZezula\WP\Plugin\MultiSite\ApproveSite\Model\RegistrationValidator',
    'TomasZezula\WP\Plugin\MultiSite\ApproveSite\Model\Registration'
  );

  foreach ($required_classes as &$required_class) {
    if (!class_exists($required_class)) {
        die('Class '.$required_class.' doesn\'t exist');
    }
  }

  use TomasZezula\WP\Plugin\MultiSite\ApproveSite\ApproveSitePlugin as ApproveSitePlugin;
  use TomasZezula\WP\Plugin\MultiSite\ApproveSite\Settings as Settings;
  use TomasZezula\WP\Plugin\MultiSite\ApproveSite\TemplateHandler as TemplateHandler;

  $plugin_templates = array(
    'registration.php' => 'WPMU Registration',
    'registration-approve.php' => 'WPMU Registration Approve',
    'registration-reject.php' => 'WPMU Registration Reject'
  );

  $template_handler = new TemplateHandler($plugin_templates);

  $approveSitePlugin = new ApproveSitePlugin($template_handler);

  $pluginSettings = new Settings();


  // Activate
  register_activation_hook(__FILE__, array($approveSitePlugin, 'activate'));

  // Deactivate
  register_deactivation_hook(__FILE__, array($approveSitePlugin, 'deactivate'));

  // Uninstall - TODO

  // Create a custom plugin settings menu
  add_action('admin_menu', array($pluginSettings, 'add_menu'));

?>
