<?php

namespace New_Site_Approve;

/**
 *
 * This class provides pages needed for the custom registration.
 *
 * @since      1.0.0
 * @package    New_Site_Approve
 * @subpackage New_Site_Approve/includes
 * @author     Tomas Zezula <tomas.zezula@protonmail.com>
 */
class Plugin_Context {

	private $registration_form;

	private $registration_approve_page;

	private $registration_reject_page;

	function __construct(
		Registration_Page $registration_form,
		Registration_Page $registration_approve_page,
		Registration_Page $registration_reject_page
	) {
    $this->registration_form         = $registration_form;
    $this->registration_approve_page = $registration_approve_page;
    $this->registration_reject_page  = $registration_reject_page;
  }

	function get_registration_form() {
		return $this->registration_form;
	}

	function get_registration_approve_page() {
		return $this->registration_approve_page;
	}

	function get_registration_reject_page() {
		return $this->registration_reject_page;
	}

	public static function init() {
		$registration_form = new Registration_Page(
			'New Registration',
			'new-site-approve-registration',
			'new_registration/registration.php'
		);
		$approve_page      = new Registration_Page(
			'Registration Approve',
			'new-site-approve-registration-approve',
			'new_registration_approve/registration-approve.php'
		);
		$reject_page       = new Registration_Page(
			'Registration Reject',
			'new-site-approve-registration-reject',
			'new_registration_reject/registration-reject.php'
		);

		return new Plugin_Context(
			$registration_form, $approve_page, $reject_page
		);
	}
}
