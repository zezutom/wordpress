<?php

namespace New_Site_Approve;

/**
 *
 * This class represents a custom registration page.
 *
 * @since      1.0.0
 * @package    New_Site_Approve
 * @subpackage New_Site_Approve/includes
 * @author     Tomas Zezula <tomas.zezula@protonmail.com>
 */
class Registration_Page {

	private $title;

	private $slug;

	private $template;

	private $id;

	function __construct($title, $slug, $template) {
        $this->title    = $title;
        $this->slug     = $slug;
        $this->template = $template;
    }

    public static function from_persisted_instance(Registration_Page $page, $id) {
		$instance = new self($page->get_title(), $page->get_slug(), $page->get_template());
		$instance->set_id($id);

		return $instance;
    }

	public function get_title() {
		return $this->title;
	}

	public function get_slug() {
		return $this->slug;
	}

	public function get_template() {
		return $this->template;
	}

	private function set_id($id) {
		$this->id = $id;
	}

	public function get_id() {
		return $this->id;
	}


}
