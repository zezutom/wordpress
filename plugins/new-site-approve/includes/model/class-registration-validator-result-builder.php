<?php
/**
 * Created by PhpStorm.
 * User: tom
 * Date: 2019-03-12
 * Time: 09:09
 */

namespace New_Site_Approve;


class Registration_Validator_Result_Builder {

	private $errors;

	public function __construct() {
		$this->errors = array();
	}

	public function add_error( $key, $message ) {
		$this->errors[$key] = $message;
	}

	public function build(): Registration_Validator_Result {
		return new Registration_Validator_Result( $this->is_success(), $this->errors );
	}

	private function is_success(): bool {
		return sizeof($this->errors) == 0;
	}
}