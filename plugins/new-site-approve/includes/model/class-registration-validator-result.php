<?php

namespace New_Site_Approve;

/**
 * Created by PhpStorm.
 * User: tom
 * Date: 2019-03-12
 * Time: 09:01
 */
class Registration_Validator_Result {

	private $success;

	private $errors;

	public function __construct($success, $errors) {
		$this->success = $success;
		$this->errors = $errors;
	}

	/**
	 * @return boolean
	 */
	public function is_success() {
		return $this->success;
	}

	/**
	 * @return array of strings
	 */
	public function get_errors() {
		return $this->errors;
	}
}