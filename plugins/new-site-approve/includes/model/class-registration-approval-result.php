<?php
/**
 * Created by PhpStorm.
 * User: tom
 * Date: 2019-03-17
 * Time: 15:37
 */

namespace New_Site_Approve;


class Registration_Approval_Result {

	private $success;

	private $errors;

	public function __construct($success, $errors) {
		$this->success = $success;
		$this->errors = $errors;
	}

	/**
	 * @return boolean
	 */
	public function is_success() {
		return $this->success;
	}

	/**
	 * @return array of strings
	 */
	public function get_errors() {
		return $this->errors;
	}

}