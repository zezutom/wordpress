<?php

namespace New_Site_Approve;

/**
 * Created by PhpStorm.
 * User: tom
 * Date: 2019-03-12
 * Time: 08:40
 */
class Registration {

	private $username;
	private $email;
	private $site;
	private $site_name;

	public function __construct($username, $email, $site, $site_name) {
		$this->username = $username;
		$this->email = $email;
		$this->site = $site;
		$this->site_name = $site_name;
	}

	/**
	 * @return string
	 */
	public function get_username() {
		return $this->username;
	}

	/**
	 * @return string
	 */
	public function get_email() {
		return $this->email;
	}

	/**
	 * @return string
	 */
	public function get_site() {
		return $this->site;
	}

	/**
	 * @return string
	 */
	public function get_site_name() {
		return $this->site_name;
	}

	/**
	 * @return string
	 */
	function get_domain() {
		$root_domain = parse_url(site_url())['host'];
		return $this->site.'.'.$root_domain;
	}
}