<?php

namespace New_Site_Approve;

/**
 * Created by PhpStorm.
 * User: tom
 * Date: 2019-03-12
 * Time: 08:40
 */
 class Email_Notification {

 	private $to;

 	private $subject;

 	private $body_template;

 	private $meta;


 	public function __construct( $to, $subject, $body_template, $meta ) {
		$this->to = $to;
		$this->subject = $subject;
		$this->body_template = $body_template;
		$this->meta = $meta;
    }

	 /**
	  * @return string
	  */
	 public function get_to() {
		 return $this->to;
	 }

	 /**
	  * @return string
	  */
	 public function get_subject() {
		 return $this->subject;
	 }

	 /**
	  * @return string
	  */
	 public function get_body_template() {
		 return $this->body_template;
	 }

	 /**
	  * @param $key
	  *
	  * @return string
	  */
	 public function get_meta( $key ) {
		 return $this->meta[$key];
	 }


 }