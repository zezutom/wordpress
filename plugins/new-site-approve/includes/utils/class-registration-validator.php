<?php

namespace New_Site_Approve;

require_once APPROVE_SITE__PLUGIN_DIR . '/includes/utils/class-registration-validator.php';
require_once APPROVE_SITE__PLUGIN_DIR . '/includes/model/class-registration-validator-result.php';
require_once APPROVE_SITE__PLUGIN_DIR . '/includes/model/class-registration-validator-result-builder.php';

/**
 * Created by PhpStorm.
 * User: tom
 * Date: 2019-03-11
 * Time: 23:44
 */

class Registration_Validator {

	public static function validate( $registration ): Registration_Validator_Result {

		if ( $registration ) {
			$result = self::get_validation_result( $registration );
		} else {
			$result = Registration_Validator_Result::no_data_error();
		}

		return $result;
	}

	private static function get_validation_result( $registration ): Registration_Validator_Result {

		$username = $registration->get_username();
		$email = $registration->get_email();

		$result_builder = new Registration_Validator_Result_Builder();

		if (strpos($username, ' ') !== FALSE) {
			$result_builder->add_error('username_space', 'Username contains an empty space');
		}

		if (empty($username)) {
			$result_builder->add_error('username_empty', 'Username is mandatory');
		}

		if (username_exists($username)) {
			$result_builder->add_error('username_exists', 'Username already exists');
		}

		if (empty($email)) {
			$result_builder->add_error('email_empty', 'Email is mandatory');
		} else {
			if (!is_email($email)) {
				$result_builder->add_error('email_format_invalid', 'Email format is invalid: ' . $email);
			}

			if (email_exists($email)) {
				$result_builder->add_error('email_exists', 'Email already exists');
			}
		}

		$registration->get_site_name();

		if (empty($registration->get_site())) {
			$result_builder->add_error('site_empty', 'Site is mandatory');
		}

		if (empty($registration->get_site_name())) {
			$result_builder->add_error('site_name_empty', 'Site name is mandatory');
		} else {
			// TODO site_name exists?
		}

		if (domain_exists($registration->get_domain(), '/')) {
			$result_builder->add_error('domain_exists', 'Domain already exists');
		}

		return $result_builder->build();
	}
}