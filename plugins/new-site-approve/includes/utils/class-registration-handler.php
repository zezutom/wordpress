<?php

require_once APPROVE_SITE__PLUGIN_DIR . 'includes/model/class-registration-result.php';

use New_Site_Approve\Registration;
use New_Site_Approve\Registration_Validator;
use New_Site_Approve\Email_Notification;
use New_Site_Approve\Registration_Result;
use New_Site_Approve\Registration_Approval_Result;
use New_Site_Approve\Registration_Rejection_Result;

/**
 * A new site registration only results into a sign-up pending admin approval.
 *
 * User: tom
 * Date: 2019-03-11
 * Time: 23:36
 */
class Registration_Handler {

	public static function on_new_registration( $request ): Registration_Result {

		if ( $request ) {
			$registration = self::get_registration( $request );
			$validator_result = Registration_Validator::validate( $registration );

			if ( $validator_result->is_success() ) {
				self::disable_user_notification_email();
				self::sign_up( $registration );
				self::notify_admin( $registration );
			} else {
				self::report_errors( $validator_result->get_errors() );
			}
		}

		// TODO
		return new Registration_Result( true, array() );
	}

	public static function approve_registration( $request ): Registration_Approval_Result {
		// TODO
		return new Registration_Approval_Result( true, array() );
	}

	public static function reject_registration( $request ): Registration_Rejection_Result {
		// TODO
		return new Registration_Rejection_Result( true, array() );
	}

	private static function get_registration( $request ): Registration {
		return new Registration(
			sanitize_text_field( $request['username'] ),
			sanitize_email( $request['email'] ),
			sanitize_text_field( $request['site'] ),
			sanitize_text_field( $request['site_name'] )
		);
	}

	private static function disable_user_notification_email(): void {
		add_filter( 'wpmu_signup_blog_notification', '__return_false' );
		add_filter( 'wpmu_signup_user_notification', '__return_false' );
	}

	/**
	 * @param Registration $registration
	 * @param $wpdb
	 */
	private static function sign_up( Registration $registration ): void {
		global $wpdb;

		wpmu_signup_blog(
			$registration->get_domain(), '/',
			$registration->get_site_name(),
			$registration->get_username(),
			$registration->get_email(),
			array()
		);
	}

	private static function report_errors( $errors ) {
		// TODO
		print_r(  $errors );
	}


	private static function notify_admin( $registration ): void {
		$email_notification = self::create_admin_notification( $registration );
		$success = Email_Sender::send( $email_notification );

		if ( !$success ) {
			// TODO configurable retry
			throw new RuntimeException('Failed to notify the admin!');
		}
	}

	private static function create_admin_notification( $registration ): Email_Notification {
		$key = self::get_activation_key( $registration );
		$to = get_option('wpmu_site_approval_admin_email');
		$subject = get_option('wpmu_site_approval_email_subject');

		if ( !$to || !$subject ) {
			throw new RuntimeException('Missing options! Has the plugin been initialized?' );
		}

		$body_template = '/public/partials/email/new-registration-notification.php';
		$approve_url = get_site_url(null, 'wpmu-registration-approve?key='.$key);
		$reject_url = get_site_url(null, 'wpmu-registration-reject?key='.$key);

		$meta = array(
			'username' => $registration->get_username(),
			'email' => $registration->get_email(),
			'domain' => $registration->get_domain(),
			'site_name' => $registration->get_site_name(),
			'approve_url' => $approve_url,
			'reject_url' => $reject_url
		);

		return new Email_Notification(
			$to, $subject, $body_template, $meta
		);
	}

	private static function get_activation_key( $registration ) {
		global $wpdb;

		return $wpdb->get_var(
			$wpdb->prepare("SELECT activation_key FROM {$wpdb->signups} WHERE user_login = %s AND user_email = %s",
				$registration->get_username(), $registration->get_email())
		);
	}
}