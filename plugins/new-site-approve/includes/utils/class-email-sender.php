<?php
/**
 * Created by PhpStorm.
 * User: tom
 * Date: 2019-03-11
 * Time: 23:47
 */

class Email_Sender {

	public static function send( $email_notification ) {
		ob_start();
		require_once( APPROVE_SITE__PLUGIN_DIR . $email_notification->get_body_template() );
		$body = ob_get_contents();
		ob_end_clean();

		$result = wp_mail(
			$email_notification->get_to(),
			$email_notification->get_subject(),
			$body,
			'Content-Type: text/html; charset=UTF-8'
		);

		// TODO capture any errors, log them and return a custom error message
		return $result;
	}
}