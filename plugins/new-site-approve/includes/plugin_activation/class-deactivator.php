<?php

namespace New_Site_Approve;

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    New_Site_Approve
 * @subpackage New_Site_Approve/includes
 * @author     Tomas Zezula <tomas.zezula@protonmail.com>
 */
class Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 *
	 * @param Plugin_Context $context
	 */
	public static function deactivate(Plugin_Context $context) {

		Plugin_Page_Handler::remove( $context->get_registration_form() );
		Plugin_Page_Handler::remove( $context->get_registration_approve_page() );
		Plugin_Page_Handler::remove( $context->get_registration_reject_page() );

		flush_rewrite_rules();
	}

}
