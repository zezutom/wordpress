<?php

namespace New_Site_Approve;

class Plugin_Page_Handler {

  public static function add( Registration_Page $page, int $parent_id = 0 ) {

    global $user_ID;

    $page_id = get_page_by_path($page->get_slug());

    if ($page_id) {
      return $page_id;
    }

    $new_post = array(
          'post_title' => 'New Site Approve: '.$page->get_title(),
          'post_content' => '',
          'post_status' => 'publish',
          'post_date' => date('Y-m-d H:i:s'),
          'post_author' => $user_ID,
          'post_type' => 'page',
          'post_name' => $page->get_slug(),
          'post_parent' => $parent_id
      );

    $page_id = wp_insert_post($new_post);

    if ($page_id) {
      update_post_meta($page_id, '_wp_page_template', $page->get_template());
    } else {
      die('Failed to create template page: '.$page->get_template());
    }

    return Registration_Page::from_persisted_instance($page, $page_id);
  }

  public static function remove( Registration_Page $page ) {

  	if ( $page->get_id() ) {
    	// Skip trash
	    wp_delete_post($page->get_id(), true);
    }
  }
}
