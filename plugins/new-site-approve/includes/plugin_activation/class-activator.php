<?php

namespace New_Site_Approve;

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    New_Site_Approve
 * @subpackage New_Site_Approve/includes
 * @author     Tomas Zezula <tomas.zezula@protonmail.com>
 */
class Activator {

	/**
	 * Creates custom registration templates.
	 *
	 * Templates:
	 * - Registration Form: It replaces the default WP registration page.
	 * - Approve Page:      This page handles admin's approval of a new site registration.
	 * - Reject Page:       This page handles admin's rejection of a new site registration.
	 *
	 * @param Plugin_Context $context
	 *
	 * @since    1.0.0
	 *
	 * @return Plugin_Context
	 */
	public static function activate(Plugin_Context $context) {

		$registration_form = Plugin_Page_Handler::add( $context->get_registration_form() );

		$approve_page = Plugin_Page_Handler::add(
			$context->get_registration_approve_page(), $registration_form->get_id()
		);

		$reject_page = Plugin_Page_Handler::add(
			$context->get_registration_reject_page(), $registration_form->get_id()
		);

		flush_rewrite_rules();

		return new Plugin_Context(
			$registration_form, $approve_page, $reject_page
		);
	}

}
