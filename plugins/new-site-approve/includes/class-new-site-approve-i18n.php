<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://www.tomaszezula.com
 * @since      1.0.0
 *
 * @package    New_Site_Approve
 * @subpackage New_Site_Approve/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    New_Site_Approve
 * @subpackage New_Site_Approve/includes
 * @author     Tomas Zezula <tomas.zezula@protonmail.com>
 */
class New_Site_Approve_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'new-site-approve',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
