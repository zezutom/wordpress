<?php

class Test_New_Site_Approve_Public extends WP_UnitTestCase {

	private $plugin_public;

	function __construct() {
		$this->plugin_public = new New_Site_Approve_Public( 'new-site-approve', '1.0.0' );
	}

	public function test_activate_plugin() {
		$this->plugin_public->activate_plugin();
		$this->verify_plugin_pages_created();
	}

	public function test_deactivate_plugin() {
		$this->plugin_public->activate_plugin();
		$main_page_id = $this->get_main_page_id();

		$this->plugin_public->deactivate_plugin();
		$this->verify_plugin_pages_removed( $main_page_id );
	}

	private function verify_plugin_pages_removed( $main_page_id ) {
		$this->assertTrue( !get_post( $main_page_id ) );
		$this->verify_nested_plugin_pages_removed( $main_page_id );
	}

	private function verify_nested_plugin_pages_removed( $parent_id ) {
		$pages = get_pages(
			array(
				'child_of' => $parent_id,
			)
		);

		$this->assertTrue( empty( $pages ) );
	}

	private function assert_page_exists( $pages, $slug ) {

		$page_filter = function ( $page ) use ( $slug) {
			return $page->post_name == $slug;
		};

		$filtered_pages = array_filter(
			$pages, $page_filter
		);

		$this->assertTrue( sizeof( $filtered_pages ) == 1);
	}

	private function get_main_page_id() {
		$page = get_page_by_path( 'new-site-approve-registration' );

		if ( ! $page ) {
			$this->fail( 'Main plugin page not found' );
		}

		return $page->ID;
	}

	private function verify_plugin_pages_created() {

		$main_page_id = $this->get_main_page_id();

		$pages        = get_pages(
			array(
				'child_of' => $main_page_id,
			)
		);

		$this->assertTrue( sizeof( $pages ) == 2 );
		$this->assert_page_exists( $pages, 'new-site-approve-registration-approve' );
		$this->assert_page_exists( $pages, 'new-site-approve-registration-reject' );
	}
}