<?php

require_once APPROVE_SITE__PLUGIN_DIR . '/includes/utils/class-registration-handler.php';

/**
 * Created by PhpStorm.
 * User: tom
 * Date: 2019-03-16
 * Time: 12:02
 */
class Test_Registration_Handler extends WP_UnitTestCase {

	/** @inheritdoc */
	public function setUp() {
		parent::setUp();
		add_option('wpmu_site_approval_admin_email', 'admin@local.dev' );
		add_option('wpmu_site_approval_email_subject', 'New Registration' );
	}

	/** @inheritdoc */
	public function tearDown(){
		parent::tearDown();
		reset_phpmailer_instance();
	}

	public function test_on_new_registration() {
		$request = $this->create_registration_request();
		Registration_Handler::on_new_registration( $request );
		$this->verify_admin_notification_sent();
	}

	private function verify_admin_notification_sent() {
		$mailer = tests_retrieve_phpmailer_instance();

		$to = get_option('wpmu_site_approval_admin_email');
		$subject = get_option('wpmu_site_approval_email_subject');

		$this->assertSame( $to, $this->get_to( $mailer ) );
		$this->assertSame( $subject, $mailer->get_sent()->subject );
		// TODO Verify email body (to, subject, links - all of that at the expected location in the DOM)
	}

	private function create_registration_request() {
		return array(
			'username' => 'john.doe',
			'email' => 'john.doe@local.test',
			'site' => 'test_site',
			'site_name' => 'Test Site Name',
		);
	}

	private function get_to( $mailer ) {
		$sent_to = $mailer->get_sent()->to;
		$this->assertTrue( sizeof( $sent_to) > 0 );
		$this->assertTrue( sizeof( $sent_to[0]) > 0 );

		return $sent_to[0][0];
	}
}