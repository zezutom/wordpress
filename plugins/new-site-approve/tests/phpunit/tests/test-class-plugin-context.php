<?php

use New_Site_Approve\Registration_Page;
use New_Site_Approve\Plugin_Context;

/**
 * Class Registration_Page_Config_Test
 *
 * Tests the registration config.
 *
 */
class Plugin_Context_Test extends WP_UnitTestCase {

	/**
	 * Verifies that the config is correctly instantiated.
	 */
	public function test_init() {

		$expected_registration_form = new Registration_Page(
			'New Registration',
			'new-site-approve-registration',
			'new_registration/registration.php'
		);
		$expected_approve_page      = new Registration_Page(
			'Registration Approve',
			'new-site-approve-registration-approve',
			'new_registration_approve/registration-approve.php'
		);
		$expected_reject_page       = new Registration_Page(
			'Registration Reject',
			'new-site-approve-registration-reject',
			'new_registration_reject/registration-reject.php'
		);

		$config = Plugin_Context::init();

		$this->assertEquals($expected_registration_form, $config->get_registration_form());
		$this->assertEquals($expected_approve_page, $config->get_registration_approve_page());
		$this->assertEquals($expected_reject_page, $config->get_registration_reject_page());
	}
}
