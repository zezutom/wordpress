<?php

require_once APPROVE_SITE__PLUGIN_DIR . '/admin/class-new-site-approve-admin.php';

class Test_New_Site_Approve_Admin extends WP_UnitTestCase {

	private $plugin_admin;

	public function __construct() {
		$this->plugin_admin = new New_Site_Approve_Admin( 'new-site-approve', '1.0.0' );
	}

	public function test_add_admin_menu() {
		$this->assertEquals(
			'toplevel_page_new-site-approve-admin-menu',
			$this->plugin_admin->add_admin_menu()
		);
	}

	public function test_add_admin_menu_settings() {
		$this->plugin_admin->add_admin_menu_settings();
		$this->assertEquals( 'admin@example.org', get_option('admin_email') );
		$this->assertEquals( 'New Registration', get_option('admin_email_subject') );
		$this->assertFalse( get_option('use_password_generator') );
	}
}