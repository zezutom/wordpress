<?php

require_once APPROVE_SITE__PLUGIN_DIR . '/includes/model/class-email-notification.php';
require_once APPROVE_SITE__PLUGIN_DIR . '/includes/utils/class-email-sender.php';
require_once APPROVE_SITE__PLUGIN_DIR . '/includes/utils/class-app-utils.php';

use New_Site_Approve\Email_Notification;

/**
 * Created by PhpStorm.
 * User: tom
 * Date: 2019-03-16
 * Time: 00:37
 */
class Test_Class_Email_Sender extends WP_UnitTestCase {

	/** @inheritdoc */
	public function tearDown(){
		parent::tearDown();
		reset_phpmailer_instance();
	}

	public function test_send() {
		$email_notification = new Email_Notification(
			'dev@local.dev',
			'New Registration',
			'/admin/partials/new-site-approve-admin-display.php',
			array()
		);

		$expected_body_path = '/tests/phpunit/tests/resources/test-admin-email-body.html';

		$success = Email_Sender::send( $email_notification );
		$this->assertTrue( $success );

		$this->verify_sent( $email_notification, $expected_body_path );
	}

	private function verify_sent( $email_notification, $expected_body_path ) {
		$mailer = tests_retrieve_phpmailer_instance();

		$this->assertSame( $email_notification->get_to(), $this->get_to( $mailer ) );
		$this->assertSame( $email_notification->get_subject(), $mailer->get_sent()->subject );

		$expected_email_body = $this->get_body( $expected_body_path );
		$actual_email_body = $this->normalize_body( $mailer->get_sent()->body );
		$this->assertSame( $expected_email_body, $actual_email_body );
	}

	private function get_to( $mailer ) {
		$sent_to = $mailer->get_sent()->to;
		$this->assertTrue( sizeof( $sent_to) > 0 );
		$this->assertTrue( sizeof( $sent_to[0]) > 0 );

		return $sent_to[0][0];
	}

	private function get_body( $path ): string {
		$body = App_Utils::get_file_content( $path );
		return $this->normalize_body( $body );
	}

	private function normalize_body( $body ): string {
		$compressed_body = preg_replace('/\s+/', '', $body);
		return $this->remove_wpnonce_tag( $compressed_body );
	}

	private function remove_wpnonce_tag( $html ): string {
		return preg_replace('/<inputtype="hidden"id="_wpnonce"[^>]+\>/i', '', $html);
	}
}