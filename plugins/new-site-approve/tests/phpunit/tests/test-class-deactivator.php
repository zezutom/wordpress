<?php

require_once APPROVE_SITE__PLUGIN_DIR . '/includes/plugin_activation/class-deactivator.php';

use New_Site_Approve\Activator;
use New_Site_Approve\Deactivator;
use New_Site_Approve\Registration_Page;
use New_Site_Approve\Plugin_Context;

class Test_Deactivator extends WP_UnitTestCase {

	public function test_deactivate() {

		$context = Activator::activate( Plugin_Context::init() );
		Deactivator::deactivate( $context );

		$this->verify_removed_page( $context->get_registration_form() );
		$this->verify_removed_page( $context->get_registration_approve_page() );
		$this->verify_removed_page( $context->get_registration_reject_page() );
	}

	private function verify_removed_page( Registration_Page $page ) {
		$this->assertTrue( $page->get_id() > 0 );
		$this->assertTrue( get_post( $page->get_id() ) == null );
	}
}