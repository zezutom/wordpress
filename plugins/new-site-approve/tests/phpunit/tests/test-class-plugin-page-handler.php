<?php

require_once APPROVE_SITE__PLUGIN_DIR . '/includes/model/class-registration-page.php';
require_once APPROVE_SITE__PLUGIN_DIR . '/includes/model/class-plugin-context.php';
require_once APPROVE_SITE__PLUGIN_DIR . '/includes/plugin_activation/class-plugin-page-handler.php';

use New_Site_Approve\Registration_Page;
use New_Site_Approve\Plugin_Context;
use New_Site_Approve\Plugin_Page_Handler;
use New_Site_Approve\Activator;

/**
 * Class Template_Handler_Test
 *
 * Tests the custom registration template handler.
 *
 */
class Plugin_Page_Handler_Test extends WP_UnitTestCase {

	/**
	 * Verifies that a template page is successfully created.
	 */
	public function test_add_template_page() {

		$context = Plugin_Context::init();

		$parent_page_id = $this->add_page_and_verify_result( $context->get_registration_form() );
		$this->add_page_and_verify_result( $context->get_registration_approve_page(), $parent_page_id);
		$this->add_page_and_verify_result( $context->get_registration_reject_page(), $parent_page_id );
	}

	/**
	 * Verifies that all template pages are successfully removed.
	 */
	public function test_remove_template_pages() {

		$context = Activator::activate( Plugin_Context::init() );
		$this->verify_template_pages_exist( $context );

		Plugin_Page_Handler::remove( $context->get_registration_form() );
		Plugin_Page_Handler::remove( $context->get_registration_approve_page() );
		Plugin_Page_Handler::remove( $context->get_registration_reject_page() );

		$this->verify_template_pages_removed( $context );
	}

	/**
	 * @param Registration_Page $page
	 * @param int $parent_page_id
	 *
	 * @return int
	 */
	private function add_page_and_verify_result( Registration_Page $page, int $parent_page_id = 0 ) {

		$persisted_page = Plugin_Page_Handler::add( $page, $parent_page_id );

		$page_id = $persisted_page->get_id();
		$post = get_post($page_id);
		$this->assertEquals($parent_page_id, $post->post_parent);

		return $page_id;
	}

	private function verify_template_pages_exist( Plugin_Context $context ) {
		$this->verify_template_page_exists( $context->get_registration_form() );
		$this->verify_template_page_exists( $context->get_registration_approve_page() );
		$this->verify_template_page_exists( $context->get_registration_reject_page() );
	}

	private function verify_template_page_exists( Registration_Page $page ) {
		$this->assertTrue( $page->get_id() > 0 );
		$this->assertTrue( get_post( $page->get_id() ) != null );
	}

	private function verify_template_pages_removed( Plugin_Context $context ) {
		$this->verify_template_page_removed( $context->get_registration_form() );
		$this->verify_template_page_removed( $context->get_registration_approve_page() );
		$this->verify_template_page_removed( $context->get_registration_reject_page() );
	}

	private function verify_template_page_removed( Registration_Page $page ) {
		$this->assertTrue( $page->get_id() > 0 );
		$this->assertTrue( get_post( $page->get_id() ) == null );
	}
}
