<?php

require_once APPROVE_SITE__PLUGIN_DIR . '/includes/model/class-registration.php';
require_once APPROVE_SITE__PLUGIN_DIR . '/includes/utils/class-registration-validator.php';

use New_Site_Approve\Registration;
use New_Site_Approve\Registration_Validator;

/**
 * Created by PhpStorm.
 * User: tom
 * Date: 2019-03-12
 * Time: 22:49
 */
class Test_Registration_Validator extends WP_UnitTestCase {

	public function test_valid_registration() {
		$registration = new Registration(
			'johndoe',
			'john@doe.com',
			'johndoe',
			'John Doe\' Personal Site'
		);

		$validator_result = Registration_Validator::validate( $registration );

		$this->assertTrue( $validator_result->is_success() );
		$this->assertTrue( sizeof( $validator_result->get_errors() ) == 0 );
	}

	public function test_mandatory_fields_missing() {
		$registrations = array(
			'username_empty' => new Registration(
				null,
				'john@doe.com',
				'johndoe',
				'John Doe\' Personal Site'
			),
			'email_empty' => new Registration(
				'johndoe',
				null,
				'johndoe',
				'John Doe\' Personal Site'
			),
			'site_empty' => new Registration(
				'johndoe',
				'john@doe.com',
				null,
				'John Doe\' Personal Site'
			),
			'site_empty' => new Registration(
				'johndoe',
				'john@doe.com',
				null,
				'John Doe\' Personal Site'
			),
			'site_name_empty' => new Registration(
				'johndoe',
				'john@doe.com',
				'johndoe',
				null
			),
		);

		foreach ( $registrations as $expected_error => $registration ) {
			$this->verify_error( $registration, $expected_error );
		}

	}

	public function test_invalid_email_format() {
		$registration = new Registration(
			'johndoe',
			'INVALID_EMAIL',
			'johndoe',
			'John Doe\' Personal Site'
		);

		$this->verify_error( $registration, 'email_format_invalid' );
	}

	public function test_email_exists() {

		$user = $this->create_random_user();

		$registration = new Registration(
			'johndoe',
			$user->user_email,
			'johndoe',
			'John Doe\' Personal Site'
		);

		$this->verify_error( $registration, 'email_exists' );
	}

	public function test_username_exists() {

		$user = $this->create_random_user();

		$registration = new Registration(
			$user->user_login,
			'test@test.com',
			'johndoe',
			'John Doe\' Personal Site'
		);

		$this->verify_error( $registration, 'username_exists' );
	}

	public function test_invalid_username_format() {
		$registrations = array(
			'username_empty' => new Registration(
				'',
				'test@test.com',
				'johndoe',
				'John Doe\' Personal Site'
			),
			'username_space' => new Registration(
				'User Name',
				'test@test.com',
				'johndoe',
				'John Doe\' Personal Site'
			),
		);

		foreach ( $registrations as $expected_error => $registration ) {
			$this->verify_error( $registration, $expected_error );
		}
	}

	public function test_multiple_errors() {
		$registration = new Registration(
			'User Name',
			'test',
			'',
			null
		);

		$validator_result = Registration_Validator::validate( $registration );

		$this->assertFalse( $validator_result->is_success() );
	}

	private function verify_error( $registration, $expected_error ) {

		$validator_result = Registration_Validator::validate( $registration );

		$this->assertFalse( $validator_result->is_success() );
		$this->assertTrue( sizeof( $validator_result->get_errors() ) == 1 );
		$this->assertTrue( array_key_exists( $expected_error, $validator_result->get_errors() ) );
	}

	private function create_random_user(): WP_User {
		$username = rand_str();
		$user_id = wp_create_user($username, 'changeme', "$username@test.com" );

		$this->assertTrue( $user_id > 0 );

		return get_user_by('ID', $user_id);
	}
}