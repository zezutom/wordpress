<?php

require_once APPROVE_SITE__PLUGIN_DIR . '/includes/plugin_activation/class-activator.php';

use New_Site_Approve\Activator;
use New_Site_Approve\Registration_Page;
use New_Site_Approve\Plugin_Context;

/**
 * Class Test_Activator
 *
 * Tests the app state after the plugin activation.
 *
 */
class Test_Activator extends WP_UnitTestCase {

	public function test_activate() {

		$context = Activator::activate( Plugin_Context::init() );

		$registration_form = $context->get_registration_form();

		$this->verify_activated_page( $registration_form );

		$this->verify_activated_page(
			$context->get_registration_approve_page(),
			$registration_form->get_id()
		);

		$this->verify_activated_page(
			$context->get_registration_reject_page(),
			$registration_form->get_id()
		);

	}

	private function verify_activated_page(Registration_Page $page, int $parent_id = 0) {

		$wp_post = get_post($page->get_id());

		if ( !$wp_post ) {
			$this->fail( 'Page not found: '.$page->get_slug() );
		}

		$this->assertEquals($parent_id, $wp_post->post_parent);
	}
}