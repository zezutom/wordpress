<html>
<head>
    <style>
        <?php require( APPROVE_SITE__PLUGIN_DIR . '/public/css/new-site-approve-public.css' ); ?>
    </style>
</head>
<body>
<div class="contents">
    <p>New site request pending your approval.</p>
    <p>
    <table>
        <tr>
            <th>Username:</th>
            <td><?php echo $email_notification->get_meta( 'username' ); ?></td>
        </tr>
        <tr>
            <th>Email:</th>
            <td><?php echo $email_notification->get_meta( 'email' ); ?></td>
        </tr>
        <tr>
            <th>Site:</th>
            <td><?php echo $email_notification->get_meta( 'domain' ); ?></td>
        </tr>
        <tr>
            <th>Site Name:</th>
            <td><?php echo $email_notification->get_meta( 'site_name' ); ?></td>
        </tr>
    </table>
    </p>
    <p>
    <div class="wrapper">
        <a href="<?php echo $email_notification->get_meta( 'approve_url' ); ?>">
            <div class="button approve">
                Approve
            </div>
        </a>
        <a href="<?php echo $email_notification->get_meta( 'reject_url' ); ?>">
            <div class="button reject">
                Reject
            </div>
        </a>
    </div>
    </p>
    <p>Buttons not working?</p>
    <ul>
        <li>
            <p>Click the link below to approve:</p>
            <a href="<?php echo $email_notification->get_meta( 'approve_url' ); ?>"><?php echo $email_notification->get_meta( 'approve_url' ); ?></a>
        </li>
        <li>
            <p>Click the link below to reject:</p>
            <a href="<?php echo $email_notification->get_meta( 'reject_url' ); ?>"><?php echo $email_notification->get_meta( 'reject_url' ); ?></a>
        </li>
    </ul>
</div>
</body>
</html>