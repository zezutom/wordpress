<?php get_header(); ?>
<html>
<head>
	<style>
		<?php require( APPROVE_SITE__PLUGIN_DIR . 'views/css/main.css' ); ?>
	</style>
</head>
<body>
<div class="contents">
	<div class="registration-form">
		<p>The new site has been activated.</p>
		<table>
			<tr>
				<th>Site:</th>
				<td><?php echo $meta['site_url'] ?></td>
			</tr>
			<tr>
				<th>Admin:</th>
				<td><?php echo $meta['username'] ?></td>
			</tr>
			<tr>
				<th>Email:</th>
				<td><?php echo $meta['email'] ?></td>
			</tr>
		</table>
	</div>
</div>
</body>
</html>
<?php get_footer(); ?>