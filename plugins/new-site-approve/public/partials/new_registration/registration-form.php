<?php
get_header();

function get_error($key, $errors) {
	return array_key_exists($key, $errors) ? $errors[$key] : '';
}
?>
<html>
<style>
	<?php require( APPROVE_SITE__PLUGIN_DIR . 'public/css/new-site-approve-public.css' ); ?>
</style>
<div class="contents">
	<div class="registration-form">
		<span class="error"><?php echo get_error('registration_failure', $errors); ?></span>
		<form method="post">
			<p>
				<span class="error"><?php echo get_error('username_space', $errors); ?></span>
				<span class="error"><?php echo get_error('username_empty', $errors); ?></span>
				<label for="username">Your Username</label>
			<div>
				<input type="text" id="username" name="username" value="<?php echo $registration->get_username(); ?>" placeholder="Enter your username">
			</div>
			</p>
			<p>
				<span class="error"><?php echo get_error('email_format_invalid', $errors); ?></span>
				<span class="error"><?php echo get_error('email_exists', $errors); ?></span>
				<label for="email">Email</label>
			<div>
				<input type="text" id="email" name="email" value="<?php echo $registration->get_email(); ?>" placeholder="Enter your email address">
			</div>
			</p>
			<p>
				<span class="error"><?php echo get_error('domain_exists', $errors); ?></span>
				<span class="error"><?php echo get_error('site_empty', $errors); ?></span>
				<label for="site">Site Domain</label>
			<div>
				<input type="text" id="site" name="site" value="<?php echo $registration->get_site(); ?>" placeholder="Enter your site domain">
			</div>
			</p>
			<p>
				<span class="error"><?php echo get_error('site_name_empty', $errors); ?></span>
				<label for="site">Site Name</label>
			<div>
				<input type="text" id="site_name" name="site_name" value="<?php echo $registration->get_site_name(); ?>" placeholder="Enter your site name">
			</div>
			</p>
			<input type="submit" name="submitButton" />
		</form>
	</div>
</div>
</html>
<?php get_footer(); ?>