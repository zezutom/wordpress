<?php

require_once( APPROVE_SITE__PLUGIN_DIR . 'includes/utils/class-registration-handler.php' );

if ( $POST ) {
	$result = Registration_Handler::on_new_registration( $POST );

	if ( $result->is_success() ) {
		require_once( APPROVE_SITE__PLUGIN_DIR . 'public/partials/registration-success.php' );
    } else {
		require_once( APPROVE_SITE__PLUGIN_DIR . 'public/partials/registration-form.php' );
    }
} else {
	require_once( APPROVE_SITE__PLUGIN_DIR . 'public/partials/registration-form.php' );
}
