<?php

if ( $GET ) {
	$result = Registration_Handler::reject_registration( $GET );

	if ( $result->is_success() ) {
		require_once( APPROVE_SITE__PLUGIN_DIR . 'public/partials/registration-reject-success.php' );
	} else {
		// TODO generic error page with explanation of what went wrong
	}
}
