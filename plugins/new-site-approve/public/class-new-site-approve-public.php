<?php

use New_Site_Approve\Activator;
use New_Site_Approve\Deactivator;
use New_Site_Approve\Plugin_Context;

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    New_Site_Approve
 * @subpackage New_Site_Approve/public
 * @author     Tomas Zezula <tomas.zezula@protonmail.com>
 */
class New_Site_Approve_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;


	/**
	 * The activated config referring to the plugin template pages.
	 *
	 * @since   1.0.0
	 * @access  private
	 * @var     Plugin_Context $context   The plugin config representing the active state
	 */
	private $context;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in New_Site_Approve_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The New_Site_Approve_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/new-site-approve-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in New_Site_Approve_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The New_Site_Approve_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/new-site-approve-public.js', array( 'jquery' ), $this->version, false );

	}

	public function activate_plugin() {
		$this->context = Activator::activate( Plugin_Context::init() );
	}

	public function deactivate_plugin() {

		if ($this->context) {
			Deactivator::deactivate( $this->context );
		}
	}

	public function view_template_page( $template ) {

		global $post;

		return $post ? $this->get_template_file( $post, $template ) : $template;
	}

	private function get_template_file( $post, $default_template ): string {
		$post_meta = get_post_meta( $post->ID, '_wp_page_template', true );
		$file_name = APPROVE_SITE__PLUGIN_DIR . '/public/partials/' . $post_meta;

		return file_exists( $file_name ) ? $file_name : $default_template;
	}
}
