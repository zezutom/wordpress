<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.tomaszezula.com
 * @since      1.0.0
 *
 * @package    New_Site_Approve
 * @subpackage New_Site_Approve/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    New_Site_Approve
 * @subpackage New_Site_Approve/admin
 * @author     Tomas Zezula <tomas.zezula@protonmail.com>
 */
class New_Site_Approve_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in New_Site_Approve_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The New_Site_Approve_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/new-site-approve-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in New_Site_Approve_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The New_Site_Approve_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/new-site-approve-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Creates a new top-level menu allowing to configure the plugin.
	 */
	public function add_admin_menu() {
		return add_menu_page('Site Registration', 'Site Registration',
			'administrator', 'new-site-approve-admin-menu', array($this, 'include_admin_menu_page') );
	}

	public function add_admin_menu_settings() {
		//register our settings
		register_setting( 'site-registration-settings-group', 'admin_email' );
		register_setting(
'site-registration-settings-group', 'admin_email_subject',
			array(
				'default' => 'New Registration',
			)
		);
		register_setting( 'site-registration-settings-group', 'use_password_generator');
	}

	private function include_admin_menu_page() {
		require_once( APPROVE_SITE__PLUGIN_DIR . 'partials/new-site-approve-admin-display.php' );
	}

}
